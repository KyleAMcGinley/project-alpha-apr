from django.urls import path
from .views import (
    show_project_list,
    show_project_detail,
    create_project,
    edit_project,
    delete_project,
    search_companies,
    show_company_list,
    show_company
)

urlpatterns = [
    path("", show_project_list, name="list_projects"),
    path("<int:id>", show_project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("edit/<int:id>", edit_project, name="edit_project"),
    path("delete/<int:id>", delete_project, name="delete_project"),
    path("search/", search_companies, name="search_companies"),
    path("companies", show_company_list, name="list_companies"),
    path("company/<int:id>", show_company, name="show_company"),
]
