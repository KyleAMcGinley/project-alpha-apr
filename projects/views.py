from django.shortcuts import render, redirect
from .models import Project, Company
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from .forms import ProjectForm
import pandas as pd
from plotly.offline import plot
import plotly.express as px

# Create your views here.


@login_required(redirect_field_name="login")
def show_project_list(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {"project_list": project_list}

    return render(request, "projects/list.html", context)


@login_required(redirect_field_name="login")
def show_project_detail(request, id):
    project = get_object_or_404(Project, id=id)
    if len(project.tasks.all()) != 0:
        qs = project.tasks.all()
        projects_data = [
            {
                "Task": x.name,
                "Start": x.start_date,
                "Finish": x.due_date,
                "Assignee": x.assignee,
            }
            for x in qs
        ]

        df = pd.DataFrame(projects_data)

        fig = px.timeline(
            df, x_start="Start", x_end="Finish", y="Task", color="Assignee"
        )
        fig.update_yaxes(autorange="reversed")
        gantt_plot = plot(fig, output_type="div")

        context = {
            "project": project,
            "plot_div": gantt_plot,
        }

    else:
        context = {"project": project}

    return render(request, "projects/detail.html", context)


@login_required(redirect_field_name="login")
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)


@login_required(redirect_field_name="user_login")
def edit_project(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect("show_project", id=id)
    else:
        form = ProjectForm(instance=project)

    context = {
        "project": project,
        "form": form,
    }
    return render(request, "projects/edit.html", context)


def delete_project(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        project.delete()
        return redirect("list_projects")

    return render(request, "projects/delete.html")


def search_companies(request):
    if request.method == "POST":
        searched = request.POST["searched"]
        companies = Company.objects.filter(name__contains=searched)

    context = {
        "searched": searched,
        "companies": companies,
    }
    return render(request, "projects/search.html", context)


def show_company_list(request):
    companies = Company.objects.all()
    context = {"companies": companies}

    return render(request, "projects/company_list.html", context)


def show_company(request, id):
    company = get_object_or_404(Company, id=id)
    context = {"company": company}

    return render(request, "projects/company_detail.html", context)
