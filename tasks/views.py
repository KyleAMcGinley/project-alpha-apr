from django.shortcuts import render, redirect
from .models import Task
from projects.models import Project
from .forms import TaskForm
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(redirect_field_name="login")
def create_task(request, id):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(False)
            task.project = get_object_or_404(Project, id=id)
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {"form": form}

    return render(request, "tasks/create.html", context)


@login_required(redirect_field_name="login")
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)

    context = {
        "task_list": task_list,
    }

    return render(request, "tasks/list.html", context)


@login_required(redirect_field_name="login")
def show_task_detail(request, id):
    task = get_object_or_404(Task, id=id)

    context = {"task": task}

    return render(request, "tasks/detail.html", context)


@login_required(redirect_field_name="login")
def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.user != task.assignee:
        return redirect("show_my_tasks")
    else:
        if request.method == "POST":
            form = TaskForm(request.POST, instance=task)
            if form.is_valid():
                form.save()
                return redirect("show_task", id=id)
        else:
            form = TaskForm(instance=task)

    context = {
        "task": task,
        "form": form,
    }

    return render(request, "tasks/edit.html", context)


@login_required(redirect_field_name="login")
def delete_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.user != task.assignee:
        return redirect("show_my_tasks")
    else:
        if request.method == "POST":
            task.delete()
            return redirect("show_my_tasks")

    return render(request, "tasks/delete.html")
