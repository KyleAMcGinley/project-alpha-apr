from django.urls import path
from .views import (
    create_task,
    show_my_tasks,
    show_task_detail,
    edit_task,
    delete_task,
)

urlpatterns = [
    path("create/<int:id>", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>", show_task_detail, name="show_task"),
    path("edit/<int:id>", edit_task, name="edit_task"),
    path("delete/<int:id>", delete_task, name="delete_task"),
]
