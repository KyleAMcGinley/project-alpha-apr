from django.db import models
from projects.models import Project
from django.contrib.auth.models import User

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    week_number = models.CharField(max_length=2, blank=True)
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)

    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )

    assignee = models.ForeignKey(
        User,
        null=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    notes = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        print(self.start_date.isocalendar()[1])
        if self.week_number == "":
            self.week_number = self.start_date.isocalendar()[1]
        super().save(*args, **kwargs)
