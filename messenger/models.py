from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class Message(models.Model):
    sender = models.ForeignKey(
        User,
        related_name="sender",
        on_delete=models.CASCADE,
    )
    receiver = models.ForeignKey(
        User,
        related_name="receiver",
        on_delete=models.CASCADE,
    )
    subject = models.CharField(max_length=150)
    content = models.TextField()
    created_on = models.DateTimeField(default=timezone.now)
