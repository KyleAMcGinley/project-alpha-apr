from .models import Message
from django.forms import forms, ModelMultipleChoiceField, ModelForm
from django.contrib.auth.models import User
from django.conf import settings


class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = [
            "subject",
            "content",
            "receiver",
        ]

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(MessageForm, self).__init__(*args, **kwargs)

        self.fields['receiver'].queryset = User.objects.exclude(username = self.user.username)
