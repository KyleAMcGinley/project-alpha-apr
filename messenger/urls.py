from django.urls import path
from .views import (
    create_message,
    show_inbox,
    show_message,
    search_inbox,
    show_list_sender, delete_message
)

urlpatterns = [
    path("create/", create_message, name="create_message"),
    path("inbox/", show_inbox, name="show_inbox"),
    path("message/<int:id>", show_message, name="show_message"),
    path("search", search_inbox, name="search_inbox"),
    path("sender/<int:id>", show_list_sender, name="show_list_sender"),
    path("delete/<int:id>", delete_message, name="delete_message"),
]
