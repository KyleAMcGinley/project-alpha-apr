from django.shortcuts import render, redirect
from .models import Message
from .forms import MessageForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

# Create your views here.


@login_required(redirect_field_name="login")
def create_message(request):
    if request.method == "POST":
        form = MessageForm(request.user, request.POST)
        if form.is_valid():
            message = form.save(False)
            message.sender = request.user
            message.save()
            return redirect("show_inbox")
    else:
        form = MessageForm(request.user)

    context = {
        "form": form,
    }

    return render(request, "messenger/create.html", context)


@login_required(redirect_field_name="login")
def show_inbox(request):
    messages = Message.objects.filter(receiver=request.user).order_by(
        "-created_on"
    )
    context = {"messages": messages}

    return render(request, "messenger/inbox.html", context)


@login_required(redirect_field_name="login")
def show_message(request, id):
    message = get_object_or_404(Message, id=id)

    context = {"message": message}

    return render(request, "messenger/detail.html", context)


@login_required(redirect_field_name="login")
def show_list_sender(request, id):
    user_sender = get_object_or_404(Message, id=id).sender
    messages = Message.objects.filter(
        sender=user_sender, receiver=request.user
    ).order_by("-created_on")

    context = {"messages": messages,
               "user_sender": user_sender}

    return render(request, "messenger/sender-list.html", context)


@login_required(redirect_field_name="login")
def delete_message(request, id):
    message = get_object_or_404(Message, id=id)
    if request.method == "POST":
        message.delete()
        return redirect("show_inbox")

    return render(request, "messenger/delete.html")


@login_required(redirect_field_name="login")
def search_inbox(request):
    if request.method == "POST":
        filter_selection = request.POST["search-bar-inbox-filter"]
        inbox_searched = request.POST["inbox-searched"]
        if filter_selection == "subject":
            results = Message.objects.filter(
                subject__contains=inbox_searched, receiver=request.user
            ).order_by("-created_on")
        elif filter_selection == "sender":
            results = Message.objects.filter(
                sender__username__contains=inbox_searched,
                receiver=request.user,
            ).order_by("-created_on")
        elif filter_selection == "date":
            all_messages = Message.objects.all()
            for message in all_messages:
                message.created_on.strftime("%m/%d/%Y")
            results = all_messages.filter(
                created_on__contains=inbox_searched, receiver=request.user
            ).order_by("-created_on")
        else:
            all_messages = Message.objects.all()
            for message in all_messages:
                message.created_on.strftime("%m/%d/%Y")
            results = (
                Message.objects.filter(
                    subject__contains=inbox_searched, receiver=request.user
                ).order_by("-created_on")
                | Message.objects.filter(
                    sender__username__contains=inbox_searched,
                    receiver=request.user,
                ).order_by("-created_on")
                | all_messages.filter(
                    created_on__contains=inbox_searched, receiver=request.user
                ).order_by("-created_on")
            )

    context = {"results": results, "searched": inbox_searched}

    return render(request, "messenger/search.html", context)
